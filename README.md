# piops

Rôles et inventaires ansible pour gérer l'infrastructure de LQDN.

Le but de ce projet est de pouvoir faciliter le travail des équipes techniques
de LQDN en développant un ensemble de recettes décrivant l'infrastructure.

## Doc ansible
Pour référence, la documentation pour la dernière version d'ansible est
[ici](https://docs.ansible.com/ansible/latest/user_guide/)

# Structure

Pour le moment, pas de master pour ansible, on va y aller doucement. En
revanche, on utilise le playbook site.yml pour installer ansible sur
l'ensemble des machines, leur permettant ensuite d'utiliser ansible-pull
via un cronjob (toutes les 15 minutes)

# Ajout de rôles

Pour ajouter un nouveau rôle, il faut le créer dans un repo git idoine et
l'ajouter en subtree dans roles/nom_du_role sur ce repo.

Un role dummy vide peut être cloné pour créer rapidement les roles
nécessaires.

ansible-pull est configuré pour mettre à jour les sub-modules quand il
s'exécute.

# Lancer le playbook

Pour lancer le playbook, ilfaut avoir récupéré le motde passe vault (ou le spécifier sur la ligne de commande). Attention à bien protéger le fichier (notamment: chown 0600). Il suffit ensuite d'utiliser la commande ansible playbook comme suit :

    ansible-playbook site.yml -b --vault-password-file '''<fichier_vault>'''
